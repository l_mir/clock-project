
library("deSolve")
library("hydroPSO")


##################################################
data = read.csv("hughes2009.csv")
#data = data[data$Tissue=="mogene_liver",]
######################################################################
## fit sine cosine model to data
######################################################################

act_list = list()
par(mfrow=c(1,1))
for(gene in c("Arntl","Nr1d1","Per2","Cry1","Dbp")){
  y <- unlist(strsplit(as.vector(subset(data$Values,data$Symbol==gene)),";"))
  x <- unlist(strsplit(as.vector(subset(data$Time,data$Symbol==gene)),";"))
  
  if (!(length(y)>10 && length(x)>10)){return(c(NA,0))}
  y = as.numeric(y)
  x = as.numeric(x)
  
  par = nls( y ~ a*((cos(x*2*pi/24-ph) + 1)^h / (m^h + (cos(x*2*pi/24-ph) + 1)^h))+c,
                   start=list(m=5,  h=2,  ph=6,  a=max(y), c=100),
                   lower=list(m=1,  h=1,  ph=0,  a=0, c=100),
                   upper=list(m=10, h=4, ph=24, a=2e5, c=1e4),
                   algorithm="port", trace=F,
                   control=nls.control(maxiter = 100))
  coefs <- coef(par)
  m = coefs[1]
  h = coefs[2]
  ph = coefs[3]
  a = coefs[4]
  c = coefs[5]
  #par = lm(y ~ cos(x*2*pi/24) + sin(x*2*pi/24))
  #coefs = coef(par)
  #a = coefs[2]
  #b = coefs[3]
  #c = coefs[1]
  
  act_list[[gene]] = c(m,h,ph,a,c)
  names(act_list[[gene]]) = c("m","h","ph","a","c")
  
  plot(x,y,main=c("",gene),xlab="time [h]",ylab="mRNA expression")
  t = seq(min(x),max(x),0.1)
  lines(t, a*((cos(t*2*pi/24-ph) + 1)^h / (m^h + (cos(t*2*pi/24-ph) + 1)^h))+c, col="red")
}

f_Bmal1 = function(x, scaling_factor=scf){
  m = act_list[["Arntl"]]["m"]
  h = act_list[["Arntl"]]["h"]
  ph = act_list[["Arntl"]]["ph"]
  a = act_list[["Arntl"]]["a"]
  c = act_list[["Arntl"]]["c"]
  op = a*((cos(x*2*pi/24-ph) + 1)^h / (m^h + (cos(x*2*pi/24-ph) + 1)^h))+c
  return(op)
}
f_RevErba = function(x, scaling_factor=scf){
  m = act_list[["Nr1d1"]]["m"]
  h = act_list[["Nr1d1"]]["h"]
  ph = act_list[["Nr1d1"]]["ph"]
  a = act_list[["Nr1d1"]]["a"]
  c = act_list[["Nr1d1"]]["c"]
  op = a*((cos(x*2*pi/24-ph) + 1)^h / (m^h + (cos(x*2*pi/24-ph) + 1)^h))+c
  return(op)
  }
f_Per2 = function(x, scaling_factor=scf){
  m = act_list[["Per2"]]["m"]
  h = act_list[["Per2"]]["h"]
  ph = act_list[["Per2"]]["ph"]
  a = act_list[["Per2"]]["a"]
  c = act_list[["Per2"]]["c"]
  op = a*((cos(x*2*pi/24-ph) + 1)^h / (m^h + (cos(x*2*pi/24-ph) + 1)^h))+c
  return(op)
  }
f_Cry1 = function(x, scaling_factor=scf){
  m = act_list[["Cry1"]]["m"]
  h = act_list[["Cry1"]]["h"]
  ph = act_list[["Cry1"]]["ph"]
  a = act_list[["Cry1"]]["a"]
  c = act_list[["Cry1"]]["c"]
  op = a*((cos(x*2*pi/24-ph) + 1)^h / (m^h + (cos(x*2*pi/24-ph) + 1)^h))+c
  return(op)
}
f_Dbp = function(x, scaling_factor=scf){
  m = act_list[["Dbp"]]["m"]
  h = act_list[["Dbp"]]["h"]
  ph = act_list[["Dbp"]]["ph"]
  a = act_list[["Dbp"]]["a"]
  c = act_list[["Dbp"]]["c"]
  op = a*((cos(x*2*pi/24-ph) + 1)^h / (m^h + (cos(x*2*pi/24-ph) + 1)^h))+c
  return(op)
  }

######################################################################
## functions to measure features from time course
######################################################################

find_extrema = function(timeseries,time,extrema="max"){
  extr = list()
  marker <- diff(sign(diff(timeseries)))
  duplic <- c(1, diff(timeseries)) == 0
  if (extrema=="max"){
    extr$values <- timeseries[c(F, marker < 0, F) & !duplic]
    extr$location   <- time[c(F, marker < 0, F) & !duplic]
  } else if (extrema=="min"){
    extr$values <- timeseries[c(F, marker > 0, F) & !duplic]
    extr$location   <- time[c(F, marker > 0, F) & !duplic]
  }
  return(extr)
}

find_zeros = function(timeseries,time){
  marker <- which(c(diff(sign(timeseries)) != 0, F))
  
  # determine using linear interpolation
  slopes <- (timeseries[marker+1]-timeseries[marker]) / (time[marker+1]-time[marker])
  intercepts <- timeseries[marker] - slopes * time[marker]
  zeros <- -intercepts/slopes
  
  return(zeros)
}

circadian_measures = function(timeseries,time){
  ##################################################
  ## Compute and return features of the timeseries:
  ## period, phases, fold changes.
  ## The phases are computed relative to the Bmal1
  ## peak.
  ##################################################
  phases       <- rep(0,length(timeseries))
  fold_changes <- rep(0,length(timeseries))
  
  # determine mean values
  mean_values <- sapply(timeseries, mean)
  rel_means   <- c(1, mean_values[1] / mean_values[2:5])
  
  # find extrema
  maxima <- find_extrema(timeseries[[1]], time, extrema = "max")
  minima <- find_extrema(timeseries[[1]], time, extrema = "min")
  
  amp_bmal1     <- abs(max(maxima$values)-min(minima$values))
  fold_changes[1] <- max(maxima$values)/min(minima$values)
  
  # not rhythmic ?
  if (length(maxima$location)==0 | length(minima$location)==0 | amp_bmal1 < 1e-3){
    # return this when the first gene is not rhythmic, assuming that either all genes are rhythmic or none
    return(list(period=0,phases=NA,amplitudes=NA,peak_widths=NA,mean_values=NA,fold_changes=NA))
  }
  
  period <- mean(diff(maxima$location))
  
  # determine Bmal1 phase
  # (biggest maximum as phase marker, crossing of center line for peak width determination)
  center = mean(c(max(maxima$values), min(minima$values)))
  left_side_min      <- min(minima$location[minima$values < center])
  phs_biggest_max    <- min(maxima$location[maxima$location > left_side_min & maxima$values > max(maxima$values)-0.01])
  
  bmal_phase     <- phs_biggest_max %% period
  
  # iterate for all other genes (besides Bmal1)
  for(i in 2:length(timeseries)){
    maxima <- find_extrema(timeseries[[i]], time, extrema = "max")
    minima <- find_extrema(timeseries[[i]], time, extrema = "min")
    
    amp_gene <- abs(max(maxima$values)-min(minima$values))
    fold_changes[i] <- max(maxima$values)/min(minima$values)
    
    if (length(maxima$location)==0 | length(minima$location)==0 | amp_gene < 1e-3){
      # return this when the first gene is not rhythmic, assuming that either all genes are rhythmic or none
      return(list(period=0,phases=NA,amplitudes=NA,peak_widths=NA,mean_values=NA,fold_changes=NA))
    }
    
    # determine phases relative to Bmal1
    # (biggest maximum as phase marker, crossing of center line for peak width determination)
    center = mean(c(max(maxima$values), min(minima$values)))
    left_side_min      <- min(minima$location[minima$values < center])
    phs_biggest_max    <- min(maxima$location[maxima$location > left_side_min & maxima$values > max(maxima$values)-0.01])
    
    gene_phase     <- phs_biggest_max %% period
    phases[i]      <- max(gene_phase - bmal_phase, (gene_phase + period - bmal_phase) %% period)
  }
  
  fold_changes <- log2(fold_changes)
  
  if (any(!is.finite(fold_changes))){
    return(list(period=0,phases=NA,amplitudes=NA,peak_widths=NA,mean_values=NA,fold_changes=NA))
  }
  
  return(list(period=period,phases=phases,fold_changes=fold_changes))
}

######################################################################
## measure features of the fitted data
######################################################################

times = seq(1,100,by=0.1)
objective_measures <- circadian_measures(list(f_Bmal1(times),
                                              f_RevErba(times),
                                              f_Per2(times),
                                              f_Cry1(times),
                                              f_Dbp(times)), times)

print(objective_measures)

######################################################################
## set lower and upper bounds for optimization
######################################################################

###----- parameter names
par_names=c("e_bmal1","e_reva","e_per2","e_cry1","e_dbp",
            "d1","d2","d3","d4","d5", 
            "ar1","ar4",
            "cr2","cr3","cr4","cr5",
            "gr2","gr3","gr4","gr5", 
            "b_RevErb","ba2","b_Per2","ba3","b_Cry1","ba4","b_Dbp","ba5",
            "fa2","f_RevErbA","fa3","d_Per2","fa4","d_Cry1")

data_means = c(mean(f_Bmal1(seq(1,25,0.1))),
               mean(f_RevErba(seq(1,25,0.1))),
               mean(f_Per2(seq(1,25,0.1))),
               mean(f_Cry1(seq(1,25,0.1))),
               mean(f_Dbp(seq(1,25,0.1))))

# lower=c(del1=0,del2=0,del3=0,del4=0,del5=0,
#         d1=0,d2=0,d3=0,d4=0,d5=0, 
#         ar1=0.1*data_means[2],ar4=0.1*data_means[2],
#         cr2=0.1*data_means[3],cr3=0.1*data_means[3],cr4=0.1*data_means[3],cr5=0.1*data_means[3],
#         gr2=0.1*data_means[4],gr3=0.1*data_means[4],gr4=0.1*data_means[4],gr5=0.1*data_means[4], 
#         b_RevErb=1,ba2=0.1*data_means[1],b_Per2=1,ba3=0.1*data_means[1],b_Cry1=1,ba4=0.1*data_means[1],b_Dbp=1,ba5=0.1*data_means[1],
#         fa2=0.1*data_means[5],f_RevErbA=1,fa3=0.1*data_means[5],d_Per2=1,fa4=0.1*data_means[5],d_Cry1=1)
# upper=c(del1=6,del2=6,del3=6,del4=6,del5=6,
#         d1=1,d2=1,d3=1,d4=1,d5=1, 
#         ar1=10*data_means[2],ar4=10*data_means[2],
#         cr2=10*data_means[3],cr3=10*data_means[3],cr4=10*data_means[3],cr5=10*data_means[3],
#         gr2=10*data_means[4],gr3=10*data_means[4],gr4=10*data_means[4],gr5=10*data_means[4], 
#         b_RevErb=5,ba2=10*data_means[1],b_Per2=5,ba3=10*data_means[1],b_Cry1=5,ba4=10*data_means[1],b_Dbp=5,ba5=10*data_means[1],
#         fa2=10*data_means[5],f_RevErbA=5,fa3=10*data_means[5],d_Per2=5,fa4=10*data_means[5],d_Cry1=5)


parms<-read.table('no_mean_hughes_novfo_hillcos_2018-4-25_result_params_liver_4.csv')
parameters <- c(e_bmal1 = 0.31, e_cry1 = 1.25, e_per2 = 0.82, e_rev = 2.5, e_dbp = 0.37, deg1 = parms$Param6, deg2 = parms$Param7, deg3 = parms$Param8, deg4 = parms$Param9, deg5 = parms$Param10,
                ar1 = parms$Param11, ar4 = parms$Param12, cr2 = parms$Param13, cr3 = parms$Param14, cr4 = parms$Param15, cr5 = parms$Param16,
                gr2 = parms$Param17, gr3 = parms$Param18, gr4 = parms$Param19, gr5 = parms$Param20, b_RevErb = parms$Param21,
                ba2 = parms$Param22, b_Per2 = parms$Param23, ba3 = parms$Param24, b_Cry1 = parms$Param25 , ba4 = parms$Param26,
                b_Dbp = parms$Param27, ba5 = parms$Param28 , fa2 = parms$Param29, f_RevErbA =parms$Param30 , fa3 = parms$Param31, d_Per2 = parms$Param32,
                fa4 = parms$Param33, d_Cry1 = parms$Param34)



#####################################################################
##         define objective function for global optimization
#####################################################################

objfun <- function(inp_params, scale_param=TRUE){
  
  # if (scale_param) {params <- inp_params*(upper-lower)+lower}
  # else {params <- inp_params}
  # 
  ##--- run simulation
  # yinit <- c(5,5,5,5,5) #data_means
  # times <- seq(from = 0, to = 2000, by = 0.1)
  #yout <- dede(y = yinit, times = times, func = retarded, parms = params, tau = params[1:5])
  yinit <- rep(1,15) #data_means
  times <- seq(0, 340, by = 0.01)
  
  
  tryCatch({
    dyn.load(paste("ODE_model", .Platform$dynlib.ext, sep=""))
    yout <- ode(yinit, times = times, func = "derivs", parms = inp_params,
                dllname = "ODE_model", initfunc = "initmod", nout = 1)
    dyn.unload(paste("ODE_model", .Platform$dynlib.ext, sep=""))},
    error=function(cond){
      # second try...
      tryCatch({
        Sys.sleep(15)
        dyn.load(paste("ODE_model", .Platform$dynlib.ext, sep=""))
        yout <- ode(yinit, times = times, func = "derivs", parms = inp_params,
                    dllname = "ODE_model", initfunc = "initmod", nout = 1)
        dyn.unload(paste("ODE_model", .Platform$dynlib.ext, sep=""))},
        error=function(cond){
          cat(paste0(paste(inp_params,collapse="\t"),"\n"), file = "sim_errors.log", append = TRUE)
          return(1e12)
        })
    })
  
  ##--- pick data after transient
  # take_dat = (dim(yout)[1]-300*10):dim(yout)[1]
  # synth_data = yout[take_dat,]
  # time = synth_data[,1]
  # 
  ##--- determine features for scoring
  # circ = circadian_measures(list(synth_data[,2],
  #                                synth_data[,5],                        
  #                                synth_data[,8],
  #                                synth_data[,11],                  
  #                                synth_data[,14]),time)
  out_c = data.frame(BMAL1 = yout[,'1'], RevErba = yout[,'4'], PER2 = yout[,'7'], CRY1= yout[,'10'], DBP =yout[,'13'])
  
  beg_sim = 28000
  circ = circadian_measures(list(out_c$BMAL1[beg_sim:length(out_c$BMAL1)],
                                 out_c$RevErba[beg_sim:length(out_c$RevErba)],
                                 out_c$PER2[beg_sim:length(out_c$PER2)],
                                 out_c$CRY1[beg_sim:length(out_c$CRY1)],
                                 out_c$DBP[beg_sim:length(out_c$DBP)]),
                                 times[beg_sim:length(times)])
  
  
  print(circ)
  print(objective_measures)
  
  ##--- scoring function tolerances
  tol_period = 0.1
  tol_phase  = 1
  tol_fold   = c(0.1, 0.4, 0.2, 0.1, 0.12)
  
  score = sum((objective_measures$period - circ$period)^2 / tol_period^2,
              sum((objective_measures$phases - circ$phases)^2) / tol_phase^2,
              sum((objective_measures$fold_changes - circ$fold_changes)^2 / tol_fold^2))
  
  if (!is.finite(score)){
    score = 1e12
  }
  
  print(score)
  
  return(score)
}

objfun(parameters)

############################################################################
##                        run global optimization
##     (using the package hydroPSO for particle swarm optimization)
############################################################################

guess <- runif(34,lower,upper)

start.time <- Sys.time()

outp = hydroPSO(fn=objfun,
                par=parameters,
                lower=rep(1e-8, 34),
                upper=rep(1, 34),
                method="ipso",
                control=list(MinMax="min",maxit=1500,npart=45,write2disk=F,REPORT=50,parallel="parallel",
                             use.CF=T,use.IW=F,c1=2.05,c2=2.05,Xini.type="lhs", Vini.type="lhs2011",
                             topology="gbest",normalise=F)) #,k=11,abstol=10,reltol=1e-3))

end.time <- Sys.time()
time.taken <- end.time - start.time
print(time.taken)

result_params = outp$par

print(objfun(result_params, scale_param=F))










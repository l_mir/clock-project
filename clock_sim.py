
#%%
from scipy.integrate import odeint
from math import cos, radians
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate



def xyz(t):
    return (1.98*np.cos(2*3.14*t/24) - 0.6*np.sin(2*3.14*t/24) + 6.4)**4

def reva(t):
    return (-0.47*np.cos(2*3.14*t/24) + 4.60*np.sin(2*3.14*t/24) + 2.31)**4

def circa3 (w,t,xyz, d,e):
    y,z = w
    f =[xyz(t)-d*y,
    y-e*z]
    return f

t = np.linspace(0,72,1000)


xnew = xyz(t)/(sum(xyz(t))/len(xyz(t)))
revn = reva(t)/(sum(reva(t))/len(reva(t)))


y0 = 0
z0 = 0
w0 = [y0,z0]
xave = (sum(xyz(t))/len(xyz(t)))
y, z = odeint(circa3, w0, t, args=(reva,1,1)).T
ynew = y/(sum(y)/len(y))
znew = z/(sum(z)/len(z))
plt.plot(t,ynew,'b')
plt.plot(t,znew,'g')
plt.plot(t,revn,'r') 
#plt.show()
plt.close()


def findpeak(time_ser, t):
    maxind = time_ser.argmax(axis=0)
    leftind = maxind-1
    rightind = maxind+1
    yax = np.array([time_ser[leftind], time_ser[maxind], time_ser[rightind]])
    xax = np.array([t[leftind], t[maxind], t[rightind]])

    zz = np.polyfit(xax, yax, 2)
    p = np.poly1d(zz)
    nxax= np.arange(t[leftind],t[rightind], 0.001)
    nyax = p(nxax)
    nind = nyax.argmax(axis=0)

    return nxax[nind]
#Write a function for the loop for params K,d,e,Xnew, etc.

def circaloop(w):
    sim_step = 10000
    t = np.linspace(0,500,sim_step)
    half_l = sim_step/2
    
    xnew = xyz(t)/(sum(xyz(t))/len(xyz(t)))

    K = 1.1
    pdiff = []
    pdiff2 = []
    deglist= []
    zxlist = []
    yxlist = []
    while(K<20):
        if(w==1):
            d = 2/(K)
            e = d
        if(w==2):
            d = 3/K
            e = 3/(2*K)
        if(w==3):
            d = 3/(2*K)
            e = 10/K
            
        y0 = 0
        z0 = 0
        w0 = [y0,z0]
        xave = (sum(xyz(t))/len(xyz(t)))
        y, z = odeint(circa3, w0, t, args=(xyz,d,e)).T
        ynew = y/(sum(y)/len(y))
        znew = z/(sum(z)/len(z))
        peak_diff1 = findpeak(znew,t)%24 - findpeak(xnew,t)%24+24
        pkdiff2 = findpeak(ynew,t)%24 - findpeak(xnew,t)%24+24
        pdiff.append(peak_diff1%24)
        pdiff2.append(pkdiff2%24)
        deglist.append(K)
        zampl = abs(max(znew[half_l:]) - min(znew[half_l:]))
        xampl = abs(max(xnew[half_l:]) - min(xnew[half_l:]))
        yampl = abs(max(ynew[half_l:]) - min(ynew[half_l:]))
        yx = yampl/xampl
        zx = zampl/xampl
        zxlist.append(zx)
        yxlist.append(yx)
        K=K+1
    return [deglist,zxlist,yxlist]
## simulate for different d and e to look for amplitude increase. 
# w = 1 for d=e, w=2 for e*2=d , w=3 for e = 6*d

outp1 = circaloop(2)
deglist = outp1[0]
zx2 = outp1[1]
yxlist = outp1[2]

'''
plt.plot(deglist, zxlist, 'r', label = 'Zamp/Xamp')
plt.plot(deglist, yxlist, 'b--', label = 'Yamp/Xamp')
plt.title('degradation :2* e = d')
plt.legend()
#plt.show()
'''
'''
zx2 = outp1[1]
e1d = circaloop(1)
e2d = circaloop(3)
zx1 = e1d[1]
zx3 = e2d[1]

plt.plot(deglist, zx1, 'r', label = 'e=d')
plt.plot(deglist, zx2, 'b', label = 'e=2*d')
plt.plot(deglist, zx3, 'g', label= 'e=6*d')
plt.legend()
plt.show()
'''

'''
plt.plot(deglist,pdiff, label="zpeak-xpeak")
plt.plot(deglist,pdiff2, label='ypeak-xpeak')
plt.xlabel('1/deg')
plt.legend()
#plt.show()
#Plotting xyz
 '''
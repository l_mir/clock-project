require(deSolve)
parms<-read.table('no_mean_hughes_novfo_hillcos_2018-4-25_result_params_liver_4.csv')
#deg rates for paramset 1  : e_bmal1 = 0.31, e_cry1 = 1.25, e_per2 = 0.82, e_rev = 2.5, e_dbp = 0.37 

parms<-read.table('no_mean_hughes_novfo_hillcos_2018-4-25_result_params_liver_13.csv')
parms<-read.table('no_mean_hughes_novfo_hillcos_2018-4-25_result_params_liver_10.csv')
# deg rates for paramset 3 : e_cry1 = 0.32, e_per2 = 1.15, e_rev = 0.55, e_dbp = 0.26

core_circa <- function(t, state, parameters) {
  with(as.list(c(state, parameters)), {
    
    dBMAL1 = (1.0+Zrev/(ar1))^(-2.0) - deg1*BMAL1
    dYbmal1 = BMAL1 - e_bmal1 * Ybmal1
    dZbmal1 = Ybmal1 - e_bmal1 * Zbmal1
    
    dReva = ((1.0+b_RevErb*Zbmal1/(ba2))/(1.0+Zbmal1/(ba2)))^3.0 * (1.0+Zper2/(cr2))^(-3.0) * (1.0+Zcry1/(gr2))^(-3.0) *((1.0+f_RevErbA*Zdbp/(fa2))/(1.0+Zdbp/(fa2))) - deg2*Reva
    dYrev = Reva - e_rev*Yrev
    dZrev = Yrev - e_rev*Zrev
    
    dPer2 = ((1.0+b_Per2*Zbmal1/(ba3))/(1.0+Zbmal1/(ba3)))^2.0 * (1.0+Zper2/(cr3))^(-2.0) * (1.0+Zcry1/(gr3))^(-2.0)* ((1.0+d_Per2*Zdbp/(fa3))/(1.0+Zdbp/(fa3))) - deg3*Per2
    dYper2 = Per2 - e_per2*Yper2
    dZper2 = Yper2 - e_per2*Zper2
    
    dCry1 = ((1.0+b_Cry1*Zbmal1/(ba4))/(1.0+Zbmal1/(ba4)))^2.0 * (1.0+Zper2/(cr4))^(-2.0) * (1.0+Zcry1/(gr4))^(-2.0)* (1.0+Zrev/(ar4))^(-2.0) * ((1.0+d_Cry1*Zdbp/(fa4))/(1.0+Zdbp/(fa4))) - deg4*Cry1
    dYcry1 = Cry1 - e_cry1*Ycry1
    dZcry1 = Ycry1 - e_cry1*Zcry1
    
    dDbp = ((1.0+b_Dbp*Zbmal1/(ba5))/(1.0+Zbmal1/(ba5)))^3.0 * (1.0+Zper2/(cr5))^(-3.0) * (1.0+Zcry1/(gr5))^(-3.0) - deg5*Dbp
    dYdbp = Dbp - e_dbp*Ydbp
    dZdbp = Ydbp - e_dbp*Zdbp
    
    list(c(dBMAL1, dYbmal1, dZbmal1, dReva, dYrev, dZrev, dPer2, dYper2, dZper2, dCry1, dYcry1, dZcry1, dDbp, dYdbp, dZdbp))
  })
}

parameters <- c(deg1 = parms$Param6, deg2 = parms$Param7, deg3 = parms$Param8, deg4 = parms$Param9, deg5 = parms$Param10,
                ar1 = parms$Param11, ar4 = parms$Param12, cr2 = parms$Param13, cr3 = parms$Param14, cr4 = parms$Param15, cr5 = parms$Param16,
                gr2 = parms$Param17, gr3 = parms$Param18, gr4 = parms$Param19, gr5 = parms$Param20, b_RevErb = parms$Param21,
                ba2 = parms$Param22, b_Per2 = parms$Param23, ba3 = parms$Param24, b_Cry1 = parms$Param25 , ba4 = parms$Param26,
                b_Dbp = parms$Param27, ba5 = parms$Param28 , fa2 = parms$Param29, f_RevErbA =parms$Param30 , fa3 = parms$Param31, d_Per2 = parms$Param32,
                fa4 = parms$Param33, d_Cry1 = parms$Param34, e_bmal1 = 0.31, e_cry1 = 1.25, e_per2 = 0.82, e_rev = 2.5, e_dbp = 0.37)



state      <- c(BMAL1 = 3.5, Ybmal1 = 2.5, Zbmal1 =0.5, Reva=0.5, Yrev =0.5, 
                Zrev=0.5,  Per2=0.5, Yper2= 0.5, Zper2 = 0.5, Cry1=0.5, Ycry1=0.5, Zcry1 = 0.5, Dbp=3, Ydbp =0.5, Zdbp =0.5)
times      <- seq(0, 340, by = 0.01)
out <- ode(y = state, times = times, func = core_circa, parms = parameters)
out_core = data.frame(BMAL1 = out[,'BMAL1'], RevErba = out[,'Reva'], PER2 = out[,'Per2'], CRY1= out[,'Cry1'], DBP =out[,'Dbp'])

par(mfrow = c(1,1))
matplot(out[,'time'], out_core, type= 'l')
legend('top',colnames(out_core), col=seq_len(ncol(out_core)),cex=0.8,fill=seq_len(ncol(out_core)))

find_extrema = function(timeseries,time,extrema="max"){
  extr = list()
  marker <- diff(sign(diff(timeseries)))
  duplic <- c(1, diff(timeseries)) == 0
  if (extrema=="max"){
    extr$values <- timeseries[c(F, marker < 0, F) & !duplic]
    extr$location   <- time[c(F, marker < 0, F) & !duplic]
  } else if (extrema=="min"){
    extr$values <- timeseries[c(F, marker > 0, F) & !duplic]
    extr$location   <- time[c(F, marker > 0, F) & !duplic]
  }
  return(extr)
}

maxima <- find_extrema(out_core$DBP, times, extrema = "max")
mean(diff(maxima$location))



